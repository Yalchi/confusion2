import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Picker,
  Modal,
  Switch,
  ScrollView,
  Alert
} from "react-native";
import { Button } from "react-native-elements";
import DatePicker from "react-native-datepicker";
import * as Animatable from "react-native-animatable";
import { Permissions, Notifications, Calendar } from "expo";

class Reservation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guests: 1,
      smoking: false,
      date: "",
      showModal: false
    };
  }
  static navigationOptions = {
    title: "Reserve Table" 
  };

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  handleReservation() {
    console.log(JSON.stringify(this.state));
    Alert.alert(
      'This Reservation Fine?',
      'Number of Guests: '+this.state.guests+"\n"+"Smoking? "+this.state.smoking+"\n"+"Date and Time: "+this.state.date,
        [
        {text: 'Cancel', onPress: () => this.resetForm(), style: 'cancel'},
        {text: 'OK', onPress: () => {this.presentLocalNotification(this.state.date); this.addReservationToCalendar(this.state.date); this.resetForm()}, style:'default'},
        ],
      { cancelable: false }
  );
    // this.toggleModal();
  }
  resetForm() {
    this.setState({
      guests: 1,
      smoking: false,
      date: "",
      showModal: false, 
    });
  }

  async addReservationToCalendar(date) {
    await this.optainCalendarPermission();
    Calendar.createEventAsync(Calendar.DEFAULT,{
      title: "Con Fusion Table Reservation",
      startDate:  new Date(Date.parse(date)),
      endDate: new Date(Date.parse(date) + 2*60*60*1000),
      location: "121 Clear Water Bay Road, Clear Water Bay, Kowloon, Hong Kong ",
      timeZone: 'Asia/Hong_Kong'
    })
    .then( event => {
      console.log('success',event);
        })
  .catch( error => {
      console.log('failure',error);
      });
  }

  async optainNotificationPermission(){
    var permission = await Permissions.getAsync(Permissions.USER_FACING_NOTIFICATIONS)
    if (permission !== "granted"){
        permission = await Permissions.askAsync(Permissions.USER_FACING_NOTIFICATIONS)
        if (permission !== "granted"){
          Alert.alert("Permission not granted to show notifications")
        }
    }
    return permission;
  }

  async optainCalendarPermission(){
    var permission = await Permissions.getAsync(Permissions.CALENDAR)
    if (permission !== "granted"){
        permission = await Permissions.askAsync(Permissions.CALENDAR)
        if (permission !== "granted"){
          Alert.alert("Permission not granted to have access to the calendar")
        }
    }
    return permission;
  }

  async presentLocalNotification(date) {
    await this.optainNotificationPermission();
    Notifications.presentLocalNotificationAsync({
        title: 'Your Reservation',
        body: 'Reservation for '+ date + ' requested',
        ios: {
            sound: true
        },
        android: {
            sound: true,
            vibrate: true,
            color: '#512DA8'
        }
    });
  }

  render() {
    return (
      <Animatable.View animation='zoomIn' duration={1000} delay={100} 
        
      >
        <ScrollView>
          <View style={styles.formRow}>
            <Text style={styles.formLabel}>Number of Guests</Text>
            <Picker
              style={styles.formItem}
              selectedValue={this.state.guests}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ guests: itemValue })
              }
            >
              <Picker.Item label="1" value="1" />
              <Picker.Item label="2" value="2" />
              <Picker.Item label="3" value="3" />
              <Picker.Item label="4" value="4" />
              <Picker.Item label="5" value="5" />
              <Picker.Item label="6" value="6" />
            </Picker>
          </View>
          <View style={styles.formRow}>
            <Text style={styles.formLabel}>Smoking?</Text>
            <Switch
              style={styles.formItem}
              value={this.state.smoking}
              trackColor="512DA8"
              onValueChange={value => this.setState({ smoking: value })}
            />
          </View>
          <View style={styles.formRow}>
            <Text style={styles.formLabel}>Reservation Date</Text>
            <DatePicker
              styel={{ flex: 2, margin: 20 }}
              date={this.state.date}
              mode="datetime"
              placeholder="Select Date and Time"
              minDate="2019.01.01"
              format=""
              confirmBtnText="confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: "absolute",
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
              }}
              onDateChange={date => {
                this.setState({ date: date });
              }}
            />
          </View>
          <View style={styles.formRow}>
            <Button
              title="Reserve"
              trackColor="#512DA8"
              onPress={() => this.handleReservation()}
              accessibilityLabel="Learn More About This Button"
            />
          </View>
          <Modal
            animationType={"slide"}
            transparent={false}
            visible={this.state.showModal}
            onDismiss={() => {
              this.toggleModal();
              this.resetForm();
            }}
            onRequestClose={() => {
              this.toggleModal();
              this.resetForm();
            }}
          >
            <View style={styles.modal}>
              <Text style={styles.titleModal}>Your Reservation</Text>
              <Text style={styles.modalText}>
                Number of Guests: {this.state.guests}
              </Text>
              <Text style={styles.modalText}>
                Smoking: {this.state.smoking ? "Yes" : "No"}
              </Text>
              <Text style={styles.modalText}>
                Date and Time: {this.state.date}
              </Text>
              <Button
                onPress={() => {
                  this.toggleModal();
                  this.resetForm();
                }}
                color="#512DA8"
                title="Close"
              />
            </View>
          </Modal>
        </ScrollView>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  formRow: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexDirection: "row",
    margin: 20
  },
  formLabel: {
    fontSize: 18,
    flex: 2
  },
  formItem: {
    flex: 1
  },
  modal: {
    justifyContent: "center",
    margin: 20
  },
  titleModal: {
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: "#512DA8",
    textAlign: "center",
    color: "white",
    marginBottom: 20
  },
  modalText: {
    fontSize: 18,
    margin: 10
  }
});

export default Reservation;
