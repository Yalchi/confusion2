import React, { Component } from "react";
import { View, Modal, StyleSheet, Button } from "react-native";
import { Rating, Input } from "react-native-elements";

class CommentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentText: "",
      commentId: "",
      rating: 0,
      author: "",
      date: "",
      dishId: "",
      showModal: false
    };
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }
  resetForm() {
    this.setState({
      commentText: "",
      commentId: "",
      rating: 0,
      author: "",
      date: "",
      dishId: "",
      showModal: false
    });
  }
  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.state.showModal}
        onDismiss={() => {
          this.toggleModal();
          this.resetForm();
        }}
        onRequestClose={() => {
          this.toggleModal();
          this.resetForm();
        }}
      >
        <View style={styles.modal}>
          <Rating
            showRating
            type="star"
            fractions={1}
            startingValue={0}
            imageSize={40}
            onFinishRating={this.ratingCompleted}
            onStartRating={this.ratingStarted}
            style={{ paddingVertical: 10 }}
          />
          <Input
            placeholder="Author"
            leftIcon={{ type: "font-awesome", name: "user" }}
            onChangeText={text => {
              this.setState({ author: text });
            }}
          />
          <Input
            placeholder="Comment"
            leftIcon={{ type: "font-awesome", name: "comment" }}
            onChangeText={text => {
              this.setState({ commentText: text });
            }}
          />
          <Button
            onPress={() => {
              this.toggleModal();
              this.resetForm();
            }}
            color="#512DA8"
            title="Submit"
          />
          <Button
            onPress={() => this.toggleModal()}
            color="#512DA8"
            title="Cancel"
          />
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  formRow: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexDirection: "row",
    margin: 20
  },
  formLabel: {
    fontSize: 18,
    flex: 2
  },
  formItem: {
    flex: 1
  },
  modal: {
    justifyContent: "center",
    margin: 20
  },
  titleModal: {
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: "#512DA8",
    textAlign: "center",
    color: "white",
    marginBottom: 20
  },
  modalText: {
    fontSize: 18,
    margin: 10
  }
});
export default CommentComponent;
