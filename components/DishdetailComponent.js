import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Modal,
  StyleSheet,
  Button,
  Alert,
  PanResponder,
  Share
} from "react-native";
import { Card, Icon, Rating, Input } from "react-native-elements";
import { connect } from "react-redux";
import { baseUrl } from "../shared/baseUrl";
import { postFavorite } from "../redux/ActionCreators";
import { changeShowModal } from "../redux/ActionCreators";
import { postComment } from "../redux/ActionCreators";
import { comments } from "../redux/ActionCreators";
import { fetchComments } from "../redux/ActionCreators";
import * as Animatable from "react-native-animatable";

const mapStateToProps = props => {
  return {
    dishes: props.dishes,
    comments: props.comments,
    favorites: props.favorites,
    showModal: props.showModal
  };
};

const mapDispatchToProps = dispatch => ({
  postFavorite: dishId => dispatch(postFavorite(dishId)),
  postComment: comment => dispatch(postComment(comment)),
  changeShowModal: showModal => dispatch(changeShowModal(showModal))
});

function RenderDish(props) {
  const dish = props.dish;
  handleViewRef = ref => this.view = ref ;

  const recognizeDrag = ({moveX, moveY, dx, dy}) => {
      if ( dx < -200 )
            return 1;
      else if ( dx > 200 )
            return 2;
      else
            return 3;
  }
  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (e, gestureState) => {
        return true;
    },
    onPanResponderGrant: () => {
      this.view.rubberBand(800)
        .then(endState => console.log(endState.finished ? 'finished' :  'canceled'))
    },
    onPanResponderEnd: (e, gestureState) => {
      console.log("pan responder end", gestureState);
      if (recognizeDrag(gestureState) === 1 ) {
        Alert.alert(
          'Add Favorite',
          'Are you sure you wish to add ' + dish.name + ' to favorite?',
          [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => {props.favorite ? console.log('Already favorite') : props.onPress()}},
          ],
          { cancelable: false }
      );
      } else if (recognizeDrag(gestureState) === 2 ){
        props.onPressComment();
      }

      return true;
  }
  })

  const shareDish = (title, message, url) => {
    Share.share({
      title: title,
      message: title + ': ' + message + " " + url,
      url: url
    },{
      dialogTitle: "share " + title
    })
  }

  if (dish != null) {
    return (
      <Animatable.View animation="fadeInDown" duration={2000} delay={1000} {...panResponder.panHandlers}
        ref={this.handleViewRef}
      >
        <Card featuredTitle={dish.name} image={{ uri: baseUrl + dish.image }}>
          <Text style={{ margin: 10 }}>{dish.description}</Text>
          <View style={styles.dishView}>
            <Icon
              style={{ flex: 1 }}
              raised
              reverseColor
              name={props.favorites ? "heart" : "heart-o"}
              type="font-awesome"
              color="#f50"
              onPress={() =>
                props.favorites
                  ? console.log("Already Favorite")
                  : props.onPress()
              }
            />
            <Icon
              style={{ flex: 1 }}
              raised
              reverseColor
              name="pencil"
              type="font-awesome"
              color="#f50"
              onPress={() => {
                props.onPressComment();
              }}
            />
            <Icon
              style={{ flex: 1 }}
              raised
              reverseColor
              name="share"
              type="font-awesome"
              color="#51D2A8"
              onPress={() => {
                shareDish(dish.name, dish.description, baseUrl + dish.image);
              }}
            />
          </View>
        </Card>
      </Animatable.View>
    );
  } else {
    return <View />;
  }
}

function RenderComments(props) {
  const comments = props.comments;
  const renderCommentsItem = ({ item, index }) => {
    return (
      <View key={index} style={{ margin: 10 }}>
        <Text style={{ fontSize: 14 }}>{item.comment}</Text>
        <Text style={{ fontSize: 12 }}>{item.rating} Stars</Text>
        <Text style={{ fontSize: 12 }}>
          {"-- " + item.author + ", " + item.date}
        </Text>
      </View>
    );
  };
  return (
    <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
      <Card title="Comments">
        <FlatList
          data={comments}
          renderItem={renderCommentsItem}
          keyExtractor={item => item.id.toString()}
        />
      </Card>
    </Animatable.View>
  );
}

class Dishdetail extends Component {
  constructor() {
    super();
    this.state = {
      author: "",
      comment: "",
      rating: 0,
      date: "",
      showModal: false
    };
  }
  handleComment() {
    let dishId = this.props.navigation.getParam("dishId", "");
    let date = new Date();
    let commentObject = {
      comment: this.state.commentText,
      dishId: dishId,
      author: this.state.author,
      rating: this.state.rating,
      date: date,
      id: 100000
    };
    this.props.postComment(commentObject);
    this.toggleModal();
    this.resetForm();
  }

  resetForm() {
    this.setState({
      author: "",
      comment: "",
      rating: 0,
      showModal: false
    });
  }

  toggleModal() {
    this.setState({
      showModal: !this.state.showModal
    });
  }

  static navigationOptions = {
    title: "Dish Details"
  };

  render() {
    const dishId = this.props.navigation.getParam("dishId", "");

    return (
      <ScrollView>
        <RenderDish
          dish={this.props.dishes.dishes[+dishId]}
          favorites={this.props.favorites.some(el => el === dishId)}
          trigger={this.props.showModal}
          onPress={() => this.props.postFavorite(dishId)}
          onPressComment={() =>
            this.setState({
              showModal: !this.state.showModal
            })
          }
        />
        <RenderComments
          comments={this.props.comments.comments.filter(
            comment => comment.dishId === dishId
          )}
        />
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.showModal}
          onDismiss={() => {
            this.toggleModal();
            this.resetForm();
          }}
          onRequestClose={() => {
            this.toggleModal();
            this.resetForm();
          }}
        >
          <View style={styles.modal}>
            <Rating
              showRating
              type="star"
              fractions={1}
              startingValue={0}
              imageSize={40}
              onStartRating={this.ratingStarted}
              style={{ paddingVertical: 10 }}
              onFinishRating={value => {
                this.setState({ rating: value });
              }}
            />
            <Input
              placeholder="Author"
              leftIcon={{ type: "font-awesome", name: "user" }}
              onChangeText={text => {
                this.setState({ author: text });
              }}
            />
            <Input
              placeholder="Comment"
              leftIcon={{ type: "font-awesome", name: "comment" }}
              onChangeText={text => {
                this.setState({ commentText: text });
              }}
            />
            <Button
              onPress={() => {
                this.handleComment();
              }}
              color="#512DA8"
              title="Submit"
            />
            <Button
              onPress={() => {
                this.toggleModal();
                this.resetForm();
              }}
              color="#512DA8"
              title="Cancel"
            />
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  dishView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  formRow: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexDirection: "row",
    margin: 20
  },
  formLabel: {
    fontSize: 18,
    flex: 2
  },
  formItem: {
    flex: 1
  },
  modal: {
    justifyContent: "center",
    margin: 20
  },
  titleModal: {
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: "#512DA8",
    textAlign: "center",
    color: "white",
    marginBottom: 20
  },
  modalText: {
    fontSize: 18,
    margin: 10
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dishdetail);
