import * as ActionTypes from "./actionTypes";

export const comment = (
  state = {
    comment: "",
    author: "",
    date: "",
    showModal: false,
    dishId: "",
    rating: 0
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.ADD_COMMENT:
      return state.concat(action.payload);

    case ActionTypes.CHANGE_MODAL:
      return !state.showModal;

    default:
      return state;
  }
};
