import * as ActionTypes from "./ActionTypes";

export const comments = (
  state = { errMess: null, comments: [], showModal: false },
  action
) => {
  switch (action.type) {
    case ActionTypes.ADD_COMMENTS:
      return { ...state, errMess: null, comments: action.payload };

    case ActionTypes.COMMENTS_FAILED:
      return { ...state, errMess: action.payload };

    case ActionTypes.ADD_COMMENT:
      return { ...state, comments: state.comments.concat(action.payload) };

    case ActionTypes.CHANGE_MODAL:
      return { ...state, showModal: action.payload };

    default:
      return state;
  }
};

